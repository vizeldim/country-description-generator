all: merge
	java -jar saxon/saxon-he-10.2.jar -s:countries.xml -xsl:xsl/index.xsl
	java -jar saxon/saxon-he-10.2.jar -s:countries.xml -xsl:xsl/countries.xsl
	fop -xml countries.xml -xsl xsl/pdf.xsl countries.pdf
	
validate: merge
	xmllint --noout --dtdvalid validate/schema.dtd countries.xml
	jing -c validate/schema.rnc countries.xml 

merge:
	xmllint --dropdtd --noent --output countries.xml mergeCountries.xml

clean:
	rm -f html/*.html
	rm -f countries.xml 
	rm -f countries.pdf