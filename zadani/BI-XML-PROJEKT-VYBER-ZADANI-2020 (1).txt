PROJEKT - VÝBĚR ZADÁNÍ

- Pokud chcete dělat projekt samostatně:

  - Navrhněte jednoduchou XML strukturu pro zaznamenání vašeho jména,
    emailu a názvů oblastí co jste si vybrali pro zpracování do projektu.

  - Po navržení XML struktury si vyberte 4 unikátní oblasti (z
    "https://www.cia.gov/library/publications/the-world-factbook/") a
    zaznamenejte je do "well-formed" XML dokumentu, který odešlete na email
    vyučujícího.


EXTRA INFO PRO ROK 2020:

- Pro výběr oblastí a stažení dat do projektu použijte rozklikávací box
  vpravo nahoře na zmíněné úvodní stránce "The World Factbook", který
  vykresluje element "select" obsahující textový uzel "Please select a
  country to view". Je to element "select" s atributem "id" s hodnotou
  "search-place" pokud ho budete chtít identifikovat jednoznačně ve
  zdrojovém kódu stránky. Rozsah projektu nebude dostatečný pokud namísto
  toho zpracujete například jen "ONE-PAGE COUNTRY SUMMARIES" pro vybrané
  oblasti. Takový projekt pak nebude možné hodnotit, protože by nebylo
  zachováno konzistentní hodnocení oproti předchozím semestrům a nebylo by
  to férové vůči studentům co zpracují projekt ve správném rozsahu.
