<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<fo:root>
			<fo:layout-master-set>
				<fo:simple-page-master master-name="A4-portrait">
					<fo:region-body margin="2cm" />
					<fo:region-after extent="2cm" />
				</fo:simple-page-master>
			</fo:layout-master-set>

			<fo:page-sequence master-reference="A4-portrait">
				<fo:static-content flow-name="xsl-region-after" color="gray">
					<fo:block text-align="center">
						<fo:page-number/>
					</fo:block>
				</fo:static-content>

				<fo:flow flow-name="xsl-region-body" font-family="Arial,Helvetica,sans-serif" space-after="2cm">
					<fo:block-container height="100%" display-align="center">
						<fo:block font-size="33px" font-weight="bold">The World Factbook</fo:block>
						<fo:block font-size="17px" space-before="0.5cm">BI-XML Semestral Work</fo:block>
						<fo:block font-size="11px" space-before="0.5cm">Autor: Dimitri Vizelka</fo:block>
					</fo:block-container>

					<xsl:apply-templates select="/countries"/>

					<xsl:apply-templates select="/countries/country"/>
				</fo:flow>
			</fo:page-sequence>
		</fo:root>
	</xsl:template>

	<xsl:template match="/countries">
		<fo:block page-break-before="always" font-weight="bold" font-size="27px" space-before="0.5cm">
			Countries:
		</fo:block>
		<fo:list-block>	   
			<xsl:for-each select="./country">
				<fo:list-item>
					<fo:list-item-label>
						<fo:block></fo:block>
					</fo:list-item-label>

					<fo:list-item-body >
						<fo:block margin-top="10px" text-align-last="justify" font-weight="bold" font-size="17px" >
							<fo:basic-link internal-destination="{@name}">
								<xsl:value-of select="@name"/>				        		
								<fo:leader leader-pattern="dots"/>                       
								<fo:page-number-citation ref-id="{@name}"/>
							</fo:basic-link>
						</fo:block>

						<fo:list-block>
							<xsl:for-each select="./category">
								<fo:list-item>
									<fo:list-item-label>
										<fo:block>
										</fo:block>
									</fo:list-item-label>

									<fo:list-item-body>

										<fo:block text-align-last="justify" font-size="13px" margin-left="0.5cm" margin-top="5px">
											<fo:basic-link internal-destination="{../@name}-{@name}">
												<xsl:value-of select="@name"/>
												<fo:leader leader-pattern="dots" />
												<fo:page-number-citation ref-id="{../@name}-{@name}"/>
											</fo:basic-link>
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</xsl:for-each>
						</fo:list-block>
					</fo:list-item-body>
				</fo:list-item>
			</xsl:for-each>
		</fo:list-block>
	</xsl:template>

	<xsl:template match="/countries/country">
		<fo:block page-break-before="always" color="#1f62a9" font-weight="bold" font-size="33px" id="{@name}">
			<xsl:value-of select="@name"/>
		</fo:block>
		<fo:block keep-with-next="always" color="#1f62a9" font-weight="bold" font-size="20px" space-before="0.5cm">
			Flag | Location
		</fo:block>
		<fo:block>
			<fo:external-graphic src="source\images\{@name}-flag.png"  max-width="200px" content-width="scale-to-fit" scaling="uniform"/>
		</fo:block>
		<fo:block>
			<fo:external-graphic src="source\images\{@name}-locator-map.jpg"  max-width="200px" content-width="scale-to-fit" scaling="uniform"/>
		</fo:block>

		<xsl:for-each select="./category">
			<fo:block keep-with-next="always" color="#1f62a9" font-weight="bold" font-size="20px" space-before="0.7cm"  id="{../@name}-{@name}">
				<xsl:value-of select="./@name"/>
			</fo:block>
			<xsl:for-each select="./section">
				<fo:block keep-with-next="always" color="#1f62a9"  font-size="15px" space-before="0.5cm">
					<xsl:value-of select="./@name"/>
				</fo:block>

				<xsl:apply-templates select="./*" mode="normal"/>

			</xsl:for-each>
		</xsl:for-each>
</xsl:template>



<xsl:template match="numeric-info" mode="normal">
	<fo:block font-size="11px" space-before="0.1cm">
		<xsl:apply-templates select="./@name" mode="normal"/>
		<xsl:apply-templates select="." mode="same"/>
	</fo:block>
</xsl:template>

<xsl:template match="numeric-info" mode="sub">
	<xsl:apply-templates select="./@name" mode="sub"/>
	<xsl:apply-templates select="." mode="same"/>
</xsl:template>

<xsl:template match="numeric-info" mode="same">
	<xsl:value-of select="./@value"/>
	<xsl:apply-templates select="./@unit"/>
	<xsl:apply-templates select="./@year"/>
</xsl:template>


<xsl:template match="info/numeric-info">
	<xsl:apply-templates select="./@name" mode="sub"/>
	<xsl:apply-templates select="." mode="same"/>
	<xsl:text>; </xsl:text>
</xsl:template>


<xsl:template match="info" mode="normal">
	<fo:block font-size="11px" space-before="0.1cm">
		<fo:inline font-weight="bold">
			<xsl:apply-templates select="./@name" mode="normal"/>	
		</fo:inline>

		<xsl:apply-templates select="node()"/>
	</fo:block>

	<xsl:for-each select="./sub">
		<fo:block font-size="11px" space-before="0.1cm">
			<xsl:apply-templates select="./@name"/>
			<xsl:value-of select="."/>
			<xsl:apply-templates select="./numeric-info" mode="sub"/>
		</fo:block>
	</xsl:for-each>
</xsl:template>

<xsl:template match="info/text()">
	<xsl:value-of select="."/>
</xsl:template>


<xsl:template match="numeric-info/@name" mode="normal">
	<fo:inline font-weight="bold">
		<xsl:value-of select="."/>
	</fo:inline>
	<xsl:text> : </xsl:text>
</xsl:template>

<xsl:template match="numeric-info/@name" mode="sub">
	<xsl:value-of select="."/>
	<xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="numeric-info/@unit">
	<xsl:text> </xsl:text>
	<xsl:value-of select="."/>
	<xsl:text> </xsl:text>
</xsl:template>

<xsl:template match="numeric-info/@year">
	<xsl:text> ( </xsl:text>
	<xsl:value-of select="."/>
	<xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="info/@name">
	<xsl:value-of select="."/>
	<xsl:text> : </xsl:text>
</xsl:template>

<xsl:template match="sub/@name">
	<xsl:text> - </xsl:text>
	<xsl:value-of select="."/>
	<xsl:text> : </xsl:text>
</xsl:template>

</xsl:stylesheet>