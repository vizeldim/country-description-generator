# BI-XML - Semestrální práce

## Makefile
### Generate html & pdf : `make`
```
- HTML output : ./html/*.html
- PDF output : ./countries.pdf
```
### Validate dtd & relaxNG : `make validate`
### Clean all output files : `make clean`

## Merge
- xmllint --dropdtd --noent --output countries.xml mergeCountries.xml

## Validate
### DTD 
- xmllint --noout --dtdvalid validate/schema.dtd countries.xml

### RELAX NG
- jing -c validate/schema.rnc countries.xml 


## Generate 
### HTML
- java -jar saxon/saxon-he-10.2.jar -s:countries.xml -xsl:xsl/countries.xsl
- java -jar saxon/saxon-he-10.2.jar -s:countries.xml -xsl:xsl/index.xsl

### PDF
- fop -xml countries.xml -xsl xsl/pdf.xsl countries.pdf